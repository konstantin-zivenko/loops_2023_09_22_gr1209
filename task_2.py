# У реченні “Hello world” замінити всі літери “o” на “a”, а літери “l” на “e”.

string = "Hello world"

result = ""
for symbol in string:
    if symbol == "o":
        result += "a"
    elif symbol == "l":
        result += "e"
    else:
        result += symbol

print(result)


# str.maketrans(), str.translate()